import { useState, useRef } from "react";
import "./Game.css";

const Game = ({
  verifyLetter,
  pickedWord,
  pickedCategory,
  letters,
  guessedLetters,
  score,
  wrongLetters,
  guesses
}) => {

  const [letter, setLetter] = useState("");
  const letterInputRef = useRef(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    
    verifyLetter(letter)

    setLetter("");

    letterInputRef.current.focus();
  }

  return (
    <div className="game">
      <p className="points">
        <span>Points: {score}</span>
      </p>
      <h1>Guess the word:</h1>
      <h3 className="tip">
        Tipo about the word: <span>{pickedCategory}</span>
      </h3>
      <p>You still have {guesses} trie(s).</p>
      <div className="wordContainer">
        {/* <span className="letter">a</span>
        <span className="blankSquare"></span> */}
        {
          letters.map((letter, i) => (
            guessedLetters.includes(letter) ? (
              <span key={i} className="letter">{letter}</span>
            ) : (
              <span key={i} className="blankSquare"></span>
            )
          ))
        }
      </div>
      <div className="letterContainer">
        <p>Try to guess a letter of the word:</p>
        <form onSubmit={handleSubmit}>
          <input 
            type="text" 
            name="letter" 
            maxLength="1" 
            required
            onChange={(e) => setLetter(e.target.value)}
            value={letter}
            ref={letterInputRef}
          />
          <button>Play!</button>
        </form>
      </div>
      <div className="wrongLettersContainer">
        <p>Letters that you already tried: </p>
         {
          wrongLetters.map((letter, i) => (
            <span key={i}>{letter}, </span>
          ))
         }
      </div>
    </div>
  )
}

export default Game